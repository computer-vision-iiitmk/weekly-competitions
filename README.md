# weekly-competitions
This weeks competition would be focussed on edge detection and other image enhancement techniques.The problem statement would be to increase the reception of  google's tesseract 
ocr engine though this might sound trivial the reception is highly dependent on the quality of image presented to
the model.The images to be worked upon can be found inside images folder.

Evaluation will be based upon certain metrics which will be discussed later.
Submissions can be made by creating a local branch and putting a merge request to the weekly-competitions repo